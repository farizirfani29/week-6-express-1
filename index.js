import express from "express";
import fs from "fs";
import bodyParser from "body-parser";

const app = express();
const port = 5000;
app.use(bodyParser.json());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// ini untuk get all
app.get("/users", (req, res) => {
  try {
    const data = JSON.parse(fs.readFileSync("./db/db.json"));
    res.status(200);
    res.json({
      message: "Oke nich, Get all users",
      resultData: data.users,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
});

// ini untuk get using params
app.get("/users/:id", (req, res) => {
  try {
    const data = JSON.parse(fs.readFileSync("./db/db.json"));
    const userId = req.params.id;

    const resultUsers = data.users.find(
      (user) => parseInt(user.id) === parseInt(userId)
    );
    if (resultUsers) {
      res.status(200).json({
        user: resultUsers,
      });
    } else {
      res.status(400).json({
        message: "User not found",
      });
    }
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
});

// ini untuk post
app.post("/users", (req, res) => {
  try {
    const data = JSON.parse(fs.readFileSync("./db/db.json"));
    const newUser = req.body;

    const existingUser = data.users.find((user) => user.id === newUser.id);
    if (existingUser) {
      return res.status(400).json({
        message: "User ID already exists",
      });
    }

    data.users.push(newUser);

    fs.writeFileSync("./db/db.json", JSON.stringify(data, null, 2));
    res.status(201).json({
      user: newUser,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
});

// ini untuk put
app.put("/users/:id", (req, res) => {
  try {
    const data = JSON.parse(fs.readFileSync("./db/db.json"));
    const userId = req.params.id;
    const newUser = req.body;
    const resultUsers = data.users.find(
      (user) => parseInt(user.id) === parseInt(userId)
    );
    if (resultUsers) {
      const index = data.users.indexOf(resultUsers);
      data.users[index] = newUser;
      fs.writeFileSync("./db/db.json", JSON.stringify(data, null, 2));
      res.status(200).json({
        user: newUser,
      });
      return;
    } else {
      res.status(400).json({
        message: "User not found",
      });
    }
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
  res.status(202);
});

// ini untuk delete
app.delete("/users/:id", (req, res) => {
  try {
    const data = JSON.parse(fs.readFileSync("./db/db.json"));
    const userId = req.params.id;
    const resultUsers = data.users.find(
      (user) => parseInt(user.id) === parseInt(userId)
    );
    if (resultUsers) {
      const index = data.users.indexOf(resultUsers);
      data.users.splice(index, 1);
      fs.writeFileSync("./db/db.json", JSON.stringify(data, null, 2));
      res.status(200).json({
        message: "User deleted successfully",
      });
      return;
    } else {
      res.status(400).json({
        message: "User not found",
      });
    }
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
});

// untuk listen port
app.listen(port, () => {
  console.log(`Servernya jalan nich di port:${port}`);
});
